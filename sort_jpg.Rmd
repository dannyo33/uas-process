---
title: "JPG sort"
author: "Authors: Sierra Mabanta, Alex Mandel"
output:
  html_document: default
  pdf_document: default
---
project designed to move jpgs into a different folder to separate from tifs, allowing for a quicker geoprocessing time. 


```{r}
## path to directory that we want to sort
#replace path below with new folder.
#path <- "C:/Users/Public/Pictures/Flights/SampleData"
path <- "C:/Users/Public/Pictures/Flights/Cambodia/BB20180803_60"
newfolder <- file.path(path, "JPG")
setwd(path)
## listjpeg files
list_jpg <- list.files(path = path, pattern = 'jpg$|JPG$')
## separate jpegs from tifs
## make another folder for jpegs called Seq_RGB
dir.create(newfolder)
## move all jpgs to Seq_RGB
# This works for one file 
# file.rename("C:/Users/Public/Pictures/Flights/SampleData/IMG_180411_180140_0000_RGB.JPG", "C:/Users/Public/Pictures/Flights/SampleData/Seq_RGB/IMG_180411_180140_0000_RGB.JPG")
file.rename(list_jpg, file.path(newfolder,list_jpg))

```

