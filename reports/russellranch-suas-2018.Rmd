---
title: "Russell Ranch sUAS 2018"
author: "Authors: Alex Mandel, Sierra Mabanta"
date: "December 14, 2018"
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Background

Small unmanned aerial systems (sUAS) for agricultural research monitoring objectives.

*PI: Robert Hijmans; Researchers: Alex Mandel, Ani Ghosh, Travis Parker, Sierra Mabanta, Emily Hurry, Arielle Rose*

1. Comparison of commonly available multispectral sensors (5
bands) and platforms (fixed wings vs multi-rotor) for
agricultural applications.
2. Crop yield monitoring.
3. Identification of narrow-band hyperspectral wavelengths and
vegetation indices for crop mapping and growth monitoring.
4. Estimation of various soil properties and map field scale
variability of soil properties.

The majority of the work in 2018 focused on objective 1, to compare and optimize data collection with common multi-spectral sensors. Now that significant data collection has been done, we can now begin work on objectives 2 and 4 once we obtain in field measurements from other projects at Russell Ranch. Objective 3 is currently delayed as initial testing of our hyperspectral sensor has demonstrated immense challenges in data collection and processing.


```{r loading, echo=FALSE, cache=TRUE}
# Requires that you have Google Drive mounted (File Stream or Fuse(Linux))
gpath <- "/home/vulpes/gdrive/Biogeo/UAS-Flights/RussellRanch/Flight\ Data"
# Get list of finished flights
all <- list.files(gpath)

# Looking at just the processed results, there are more flights than this
results <- list.files(file.path(gpath,"Results"), recursive = TRUE)

```

```{r prepare, echo=FALSE}
# Total days we have data for
days <- all[!(all=="Results")]
# Number of flights in 2018
days <- days[days >= 2018]

tifs <- results[grep("tif$",results)]
tifs <- as.data.frame(cbind("name" = basename(tifs), "file" = tifs, "type" = dirname(tifs)), stringsAsFactors = FALSE)

# Getting the date out of the string is funky
#tifs$date <- sapply(strsplit(tifs$name,"_"), "[[", 2) # TODO keep cleaning names -Sequioa doesn't work
tifs$date <- sapply(regmatches(tifs$name, gregexpr("[[:digit:]]+", tifs$name)), "[[", 1) #Better implementation only keeps numbers                      
#Or another way
tifs$date <- gsub("[A-Z]+\\_([0-9]+).*$", "\\1", tifs$name)
tifs$date <- as.Date(tifs$date, "%Y%m%d")

```

## Methods

In 2018 we flew `r length(days)` days, at Russell Ranch. All the flights were over the Century Experiment plots. To accomodate flying all 72 acres+ (plots & paths) the flight was divided into 3 parts. This flight plan was the result of optimization between the aircraft flying time, mission planner optimization, preferred pilot location, and lowest possible altitude above ground level (AGL). 

Flights are approximately 20-24 minutes at 80m AGL, with an average speed of 8.5 m/s. Flights were conducted between 10am and 2pm. Before each flight images are captured of the multi-spectral calibration panels.


<!--The goal is to fly as low as possible to obtain as high resolution as possible while maintaining a practical altitude above ground level (AGL) for safety, and data collection within a smaller time window to minimize sun irradiance differences. We launch from the southern side, near the middle access road.This keeps the sun behind us and makes it easier to see aircraft taking off or landing at the Yolo County Airport. The center location also makes it possible to fly all 3 flights without moving personel or equipment. -->

<div>
<img src="images/flightplan.png" style="width:45%; float:left; height:250px" alt="Flight Planner"></img>
<div style="float:right; width:50%">
```{r missions, echo=FALSE, warning=FALSE, message=FALSE, cache=TRUE, fig.height=4.25}
library(sf)
library(raster)
library(RColorBrewer)

gjsonfiles <- list.files(".","*.geojson")
allsf <- lapply(gjsonfiles, read_sf)
onesf <- do.call(rbind,allsf)

onesf$label <- c("West","Mid","East")

# Load a background map
basemap <- brick(file.path(gpath,"/Results/RGB Orthomosiacs/RR_20180717_RGB_X3_transparent_mosaic_preview.png"))

# Transform projections to utm, get the centroid for labels
utmsf <- st_transform(onesf,crs(basemap, asText=TRUE))
labelpt <- do.call(rbind,(st_geometry(st_centroid(utmsf))))

# Pick the Colors
flightcol <- brewer.pal("Set1", n=5)[c(1,4,5)]
#flightcol <- c("red","green","blue")

{ 
  
  plotRGB(basemap,1,2,3)
  plot(utmsf["label"]
       , border=flightcol
       , col=NA
       , lwd=4
       , add=TRUE
       )
  text(labelpt[ ,1], labelpt[ ,2], labels=utmsf$label, col=flightcol, cex=2)
  
  #knitr::include_graphics("images/flightplan.png")
}
 

```
</div>



```{r flights, echo=FALSE, cache=TRUE, message=FALSE, include=FALSE}
#library(googlesheets)
library(rgdal)

#gdp <- gs_key("1DyJdOAWO5BGF6Dxw3sj_DgdjJhJb9ix32Toa-U6PXD8")
#gdp1 <- gs_read(gdp, ws=gs_ws_ls(gdp)[1])
# filter to Russell Ranch
#gdp1rr <- gdp1[grep("^RR",gdp1$`Flight ID`),]
#totals <- aggregate(gdp1rr$`Total number of images`, by=list((gdp1rr$`Which Sensors`)), FUN = sum)
#averages <- aggregate(gdp1rr$`Total number of images`, by=list((gdp1rr$`Which Sensors`)), FUN = mean)

# GSD isn't in the sheet, get it with gdalinfo or raster from the outputs

getRes <- function(img){
  img <- file.path(gpath,"Results",img)
  #print(img)
  info <- GDALinfo(img, silent=TRUE)
  res <- info["res.x"]
  return(res)
}
update=FALSE
#TODO: takes a while to run, need to stash the results and reload
if (!file.exists("tifs.rds") | update){
  tifs$res <- sapply(tifs$file, getRes)
  saveRDS(tifs, "tifs.rds")
} else {
  readRDS("tifs.rds")
}

```

```{r flight_summary, echo=FALSE}
# TODO: this code breaks when knitting?
# tifs$sensor <- NA
# tifs$sensor[grep("Multi.*Seq",tifs$name)] <- "Seq"
# tifs$sensor[grep("RGB.*Seq",tifs$name)] <- "RGBSeq"
# tifs$sensor[grep("Multi.*REd",tifs$name)] <- "REdge"
# tifs$sensor[grep("X3",tifs$name)] <- "X3"
# print(names(tifs))
# averages <- aggregate(tifs$res, by=list(tifs$sensor), FUN = mean)
# 
# saveRDS(averages,"averages.rds")
averages <- readRDS("averages.rds")

```
</div>

<div style="clear: both; float:none; padding-top:1em"></div>



#### Hardware

Type | Name | Bands | Average GSD |
-----|------|-------|-------------
Multi-spectral | Parrot Sequoia | 4 Band (G,R,RE,IR) | `r format(averages[3,2], digits=2)` |
Multi-spectral | Micasense Rededge | 5 Band (B,G,R,RE,IR) | `r format(averages[1,2], digits=2)` |
Color | DJI X3 | RGB | `r format(averages[4,2], digits=2)` |
Color | Parrot Sequoia | RGB | `r format(averages[2,2], digits=2)` |

* Aircraft - DJI Matrice 100
* GSD - Ground Sample Distance, size of ground represented by a singel pixel
* Only one multi-spectral camera was flown per date.

### Data Processing

Two different data processing software are used; [Pix4d](https://www.pix4d.com/product/pix4dmapper-photogrammetry-software) and [Photoscan](http://www.agisoft.com/). We are currently evaluating the difference in output products between the software. Current processing takes 1 to 4 hours, per camera, per flight.


```{r timing, echo=FALSE, fig.height=3}
#Making some timeseries plots of processed data
oldpar <- par()

par(mar=c(5,6,2.5,2))
processed <- cbind(unique(tifs$date),2)
plot(cbind(as.Date(days[-1]),2.5), ylim=c(0.25,2.51), axes=FALSE, ylab="", xlab="2018",pch=0)
title("Processing Status")
points(processed, col="black", pch=15)
multi <- cbind(tifs[grep("Multi",tifs$type),"date"],1.5)
points(multi, col="red", pch=15)
seq <- cbind(tifs[grep("Seq",tifs$name),"date"], 1.25)
points(seq, col="red", pch=16)
redge <- cbind(tifs[grep("REdg", tifs$name),"date"], 1)
points(redge, col="red", pch=17)

# TODO split into Seq vs RE
rgb <- cbind(tifs[grep("RGB O",tifs$type),"date"],.5)
points(rgb, col="blue", pch=15)

xdates <- seq(as.Date("2018-01-01"),as.Date("2018-12-31"),by="month")
axis(1, xdates, labels = format(xdates,"%b"))
axis(2, at=c(.5,1,1.25,1.5,2,2.5)
     , labels = c("RGB", "RedEdge", "Sequoia", "Multi-spectral", "Processed", "Flight Date")
     , las=2
     #, outer=TRUE
     , tick=TRUE
     , lwd = 0
     , lwd.ticks = 1
     )
#legend("topright", c("All","Multi-Spectral","Sequioa","RGB"), col=c("black","red","red","blue"), pch=c(15,15,16,15), bty="n")

#par(oldpar)

```


### Data Products

<div style="float:left; width:50%">
RGB data is combined from all 3 flights into 1 large mosaic per day. Multi-spectral data is processed as 3 separate flights. We've made sure that each plot is 100% within a single flight to make analysis easier and data more consistent.

* RGB Orthomosaics
* Reflectance Maps in 4 or 5 bands depending on sensor
* Digital Surface Models (DSM)
</div>

<div style="float:right; width:50%">
![Color Image of Russell Ranch](images/RR_20180717_RGB_X3_example.png){width=50%} ![False Color Infrared](images/RR_20180418_Multi_West_REdge_Pix_reflectance_example.png){width=30%}
</div>

<div style="clear:both">
</div>

## Results

The primary results for 2018 are the defining of flight operations methods mentioned in the methods, along with the processed data from the flights conducted this year. Processed data is now available to any researchers who request it. It should be of interest to researchers who work at Russell Ranch, and to researchers who want to work with a time series of sUAS data. There are `r nrow(tifs)` flight results, derived from more than 95,585 images, totalling more than 900 GB of data. Current processed results total 38 GB of data.

Some initial work has been started creating derivative products which are more directly useful for crop analysis.

![Examples of data analysis: Single Plot, Digital Surface Model, Greeness Index, Green Pixel Selection](images/example-analysis.png)

<!--![Single Plot](images/rgb_sub.png) ![Suface Model](images/dsm_sub.png) ![Green Cover](images/green_cover.png) ![Greeness Index Example](images/mgcvi.png)-->

## Future Plans

* Additional optimization of flights, in particular which sensor and how often.
* Comparison with last season’s ground data collected by other researchers.
* Web interface to enable researchers to view, select, and download data.
* Provide ready to analyze data to other researchers.
* Report on effectiveness of using irrigation pipes as ground control points.
