
library(rgdal)
library(raster)
library(gdalUtils)
# Requires that you have Google Drive mounted (File Stream or Fuse(Linux))
gpath <- "/home/vulpes/gdrive/Biogeo/UAS-Flights/RussellRanch/Flight\ Data"
# Get list of finished flights
all <- list.files(gpath)

# Looking at just the processed results, there are more flights than this
results <- list.files(file.path(gpath,"Results"), recursive = TRUE)

# Just the multi spectral tifs
mresults <- results[grep("Multi.*tif$",results)]

ortho <- file.path(gpath,"Results", mresults[1])
tifprop <- GDALinfo(ortho)
img <- brick(ortho)
tif1 <- gdalinfo(ortho, approx_stats = TRUE)
tif2 <- gdalinfo(ortho, approx_stats = TRUE, raw_output = FALSE)

##### Testing Speed of gdalinfo ####
library(tictoc)
testdata <- ortho[1]
testdata <- "/home/vulpes/biogeo/gfcp/uas-process/tests/RR_20180717_Multi_West_Seq_PS_reflectance.tif"

tic()
gi1 <- GDALinfo(testdata)
toc()

tic()
gi2 <- gdalinfo(testdata)
toc()

tic()
gi3 <- gdalinfo(testdata, json=TRUE)

library(jsonlite)
parsed_info <- fromJSON(paste0(gi3, collapse=""))
toc()
