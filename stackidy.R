#### Arielle Rose
#### August 23, 2018
# Purpose: Want to take individual band tif tiles from Pix4D results and stack + merge them. 
# This will avoid having to use QGIS.

####packages
#import required libraries

library(raster)
library(rgdal)

####Go into correct folder (3_dsm_ortho > 2_mosaic > tiles) 
####to select tifs... or folder (4_Index > reflectance)

stackidy <- function(pix4dlayers){

  # The base of the directory needs to be the folder with the specific name of the flight
  #pix4dlayers <- ("C:\\Users\\Public\\Pictures\\pix4D\\RusselRanch\\Complete-and-uploaded\\RR_20180425_Multi_Middle\\")
  
  setwd(pix4dlayers)
  
  ####NAME on folders are not the same!
  
  tiffiles <- list.files(path = "4_index\\reflectance", pattern = "\\.tif$",
                         full.names = TRUE)
  tiffiles <- tiffiles[c(1,2,5,4,3)]
  
  #### stack tif tiles ####
  ####create a raster stack from the input raster files 
  
  s <- raster::stack(tiffiles)
  
  ####write the output raster to file
  
  wopts <- c("COMPRESS=Deflate","PREDICTOR=2","ZLEVEL=9")
  
  stackedtif <- paste0(basename(pix4dlayers), "_merged-reflectance")
  
  r <- writeRaster(s, filename = stackedtif, format="GTiff", overwrite=TRUE, options=wopts)

  return(stackedtif)
}
