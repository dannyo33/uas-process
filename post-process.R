#Arielle Rose
#September 4, 2018
#Purpose: want a preview code to identify and separate the camera 
#types based on the number of bands. RGB has 3 bands, RedEdge has 5 
#bands, Sequoia has 4 bands.

##### Post Data Processing Master Plan ######
  # Input: folder or tiff
  # If folder {
      # stack and create one tiff
    # New input of tiff created}
  # identify number of bands to organize
    # RGB skip normalization step. Multispectral continue
  # If not normalized then normalize the reflectance
  # create preview based on number of bands
-------------
#### Library and Sources ####
#This section will add the source scripts and the library to the Environment
#This is necessarry because we will be calling upon the scripts and 
#using information that can be gained through the library.

#Always run this seciton 1st

  #library("raster")

library("rgdal")
source("previews.R")
source("stackidy.R")
source("PS_reflectance.R")

#### Input ####
#Here you will need to assign either a folder from Pix4D containing 
#the individual tiles created for the orthomosaic OR a path directory 
#to your complete orthomosaic tiff created from using Agisoft Photoscan. 
#The path directory should be to a location on the local computer.

  # Input: folder or tiff
  
#Future note: We want to make it so it will take a command line argument

ortho <- ("C:/Users/Public/Pictures/Photoscan/complete-and-uploaded/RR_20180530_Mid/RR_20180530_Multi_Mid_Seq_PS_Ortho.tif")

#### Phase One: Stack and Merge ####
#This section will be utilized if your input (ortho) was a folder from Pix4D
#If you made your input the tiff path from PS then you can skip this section 
#if you desire although there will be no harm in running it.

  # If folder
#The following line identifies if it is a folder or not. If it is it will 
#continue to next line, if not it will skip.
if (dir.exists(ortho)) {

  #then stack and create one tiff
  #The following line will run your input through stackidy script. In the script it will 
  #1. identify the tiles in the folder 
  #2. stack the individual tiles of single band to make multispectral images (in correct band order). 
  #3. merge the tiles to make a single tiff. 
  #4 return your new multispectral tiff as the new input so that it can continue to the next phase. 
  #Now you folder from Pix4d is all caught up (and technically ahead) of the PS tiff.
  ortho <- stackidy(pix4dlayers = ortho)
  
  }

#### Phase 2: Normalize ####

  # identify number of bands to organize

#The following 2 lines utilizes the library. Internally we are making it list out 
#the properties of the tiff. Here we can use and identify the band information.
tifprop <- GDALinfo(ortho)
bands <- tifprop[['bands']]

#The following will filter out the multispectral tiffs. i.e. rgb 3 band 
#tiffs will skip this phase.
if ((bands ==4) || (bands ==5)){
  print(TRUE)
  
  # If not normalized then normalize the reflectance

  #This next line will read the specific details of your tiff including
  #Bmax, GDType from the dataframe  
  tifdf <- attr(tifprop, "df")
  
  #Speciffically, if your tiff has a GDType that is an integer that can suggect 
  #that if is not normalized because if it was normalized that values would be 
  #decimal between 0 and 1 (&& means it must also have) the second condition is 
  #that the highest number (Bmax) must be greater than 3. This is because 
  #occationally a tiff will already be normalized but will have a random null 
  #value of 2 thrown in. The second line will run the tiff though the normalization
  #script if it meets the requirements.
  #The normalization scrips basically will devide all the values by the Bmax (largest
  #number) which results in values between 0 and 1. Lastly, it will change your
  #input to the new normalized tiff
  if ((tifdf$GDType=='UInt16') && (max(tifdf$Bmax) > 3)){
    ortho <- normalization(infile = ortho)
  }
  
}

#### Phase 3: Preview ####
  # create preview based on number of bands

#This section is pretty straight forward. If it has 3 bands run through the rgb script
#if 4 bands run through the sequoia script and so on. The script will produce a png
#preview in the same location as the tiff.
if ( bands ==3) {
    RGBpreview(infile = ortho)
} else if ( bands ==4) {
    Sequoia(infile = ortho)
} else if ( bands ==5) {
    RedEdge(infile = ortho)
}

